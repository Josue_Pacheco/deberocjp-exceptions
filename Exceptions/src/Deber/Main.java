package Deber;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Object o = new Object();
		Fruit f = new Fruit();
		Apple a = new Apple();
		Citrus c = new Citrus();
		Orange or = new Orange();
		Squeezable s = new Squeezable(){} ;
		
		// cast de Object
		System.out.println("\n cast de Object  \n");
		toCast(o,f);
		toCast(o,a);
		toCast(o,s);
		toCast(o,c);
		toCast(o,or);
		System.out.println("\n cast de Fruit \n");
		//cast de Fruit
		toCast(f,f);
		toCast(f,a);
		toCast(f,s);
		toCast(f,c);
		toCast(f,or);
		//cast de Applie
		System.out.println("\n cast de Apple \n");
		toCast(a,f);
		toCast(a,a);
		toCast(a,s);
		toCast(a,c);
		toCast(a,or);
		System.out.println("\n cast de Citrus \n");
		toCast(c,f);
		toCast(c,a);
		toCast(c,s);
		toCast(c,c);
		toCast(c,or);
		System.out.println("\n cast de Orange \n");
		toCast(or,f);
		toCast(or,a);
		toCast(or,s);
		toCast(or,c);
		toCast(or,or);
	}
	
	public static void toCast(Object a , Object b){
		String m = nombre(a.getClass().getName());
		String n = nombre(b.getClass().getName());
		
		try{
			
			a = a.getClass().cast(b);
			
			System.out.println( m + " -" + n + ":" + "OK ");
		}catch(Exception e){
			System.out.println(m + " - " + n+ ":NO ");
		}
		
	}
	public static String nombre(String s){
		String []x = s.split("\\.");
		int n = x.length;
		if(x[n-1].compareTo("Main$1")==0){ x[n-1]="Squeezable";	}
		return x[n-1];
		
	}
	
}
